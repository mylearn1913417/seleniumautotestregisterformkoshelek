from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from .pages.register_page import RegisterPage
from .pages.locators import *

link = "https://koshelek.ru/authorization/signup"


class TestNegativeEmptyField():
    def test_guest_can_leave_empty_fields(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.press_continue()
            page.check_exception_empty_user_name_input_field()
            page.check_exception_empty_email_input_field()
            page.check_exception_empty_password_input_field()
            page.check_exception_empty_agree_checkbox()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_dont_enter_checkbox(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_from_required_field()
            page.press_continue()
            page.check_exception_empty_agree_checkbox()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))


class TestNegativeInvalidData():

    def test_guest_can_enter_invalid_spase_required_fields(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_spase_required_fields()
            page.enter_checkbox()
            page.press_continue()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_rus_letter_capital_lowercase_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_rus_letter_capital_lowercase_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_letter_lowercase_wihtout_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_letter_lowercase_wihtout_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_letter_capital_lowercase_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_letter_capital_lowercase_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_letter_dash_symbol_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_letter_dash_and_symbol_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_spec_symbol_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_spec_symbol_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_one_letter_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_one_letter_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_one_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_one_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_exceeding_max_length_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_exceeding_max_length_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_en_many_letter_number_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_en_many_letter_number_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_emoji_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_invalid_emoji_user_name()
            page.enter_valid_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_without_at_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_without_at_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_without_domen_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_without_domen_part_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_without_local_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_without_local_part_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_with_double_at_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_with_double_at_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_reserved_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_reserved_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_nonexistent_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_nonexistent_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_with_spec_simbol_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_with_spec_simbol_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_emoji_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_emoji_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_max_length_local_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_max_length_local_part_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_exceeding_max_lenth_local_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_exceeding_max_lenth_local_part_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_max_lenth_domen_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalidl_max_lenth_domen_part_emai()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_exceeding_max_lenth_domen_part_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_invalid_exceeding_max_lenth_domen_part_email()
            page.enter_valid_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_short_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_short_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_no_required_char_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_no_required_char_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_repeating_char_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_repeating_char_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_simple_template_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_simple_template_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_spec_symbol_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_spec_symbol_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_unacceptable_symbol_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_unacceptable_symbol_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_exceeding_max_length_password(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_user_name()
            page.enter_valid_email()
            page.enter_invalid_exceeding_max_length_password()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_password_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_one_char_referal_code(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_from_required_field()
            page.enter_invalid_one_char_referal_code()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_agree_referal_code()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_exceeding_max_lenth_referal_code(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_from_required_field()
            page.enter_invalid_exceeding_max_lenth_referal_code()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_agree_referal_code()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_can_enter_invalid_many_char_referal_code(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_from_required_field()
            page.enter_invalid_referal_code_many_char()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_invalid_agree_referal_code()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))


class TestNegativeExistingData():
    def test_guest_register_existing_user_name(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_with_existing_user_name_from_required_field()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_existing_user_name_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))

    def test_guest_register_existing_email(self,browser):
        page = RegisterPage(browser, link)
        page.open()
        try:
            WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON))
            )
            page.enter_valid_data_with_existing_email_from_required_field()
            page.enter_checkbox()
            page.press_continue()
            page.check_exception_existing_email_input_field()
            page.not_go_authentication_page()
        except TimeoutException as e:
            print("Timed out while searching for item: {}".format(e))