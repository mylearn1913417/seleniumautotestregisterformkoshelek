## Набор автотестов на Python с использованием Selenium WebDriver для тестирования формы регистрации на сайте https://koshelek.ru/ по негативному сценарию.


Тесты включают негативные сценарии для следующих полей: Имя пользователя, Email, Пароль, Реферальный код, Чек-бокс.
В репозитории содержится набор тестовых данных.


Запуск тестов
1. Клонируйте репозиторий на свой локальный компьютер:\
git clone https://gitlab.com/mylearn1913417/seleniumautotestregisterformkoshelek


2. Перейдите в каталог с проектом:\
cd seleniumautotestregisterformkoshelek

3. Запустите тесты в браузере Chrome(по умолчанию запускаются в Chrome на английском), выполнив следующую команду в терминале:\
 pytest test_negative_register_form.py  


Дополнительные настройки:\
Для запуска тестов с использованием других языков добавить к команде запуска тестов --language='язык'\
Для запуска тестов в Firefox выполните в терминале команду:\
 pytest --browser_name=firefox --language=en test_negative_register_form.py\
Для запуска конкретных тестов указать их наименования, например:\
 pytest test_negative_register_form.py::TestNegativeEmptyField::test_guest_can_leave_empty_fields