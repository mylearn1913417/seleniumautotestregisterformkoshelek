from selenium.webdriver.common.by import By


class RegisterFormLocators():
    REGISTER_FORM_USER_NAME_INPUT_FIELD = (
        By.CSS_SELECTOR,
        "form.v-form > .css-grid > .k-text-field-primary > .v-input > .v-input__control > .v-input__slot > .v-text-field__slot"
    )
    REGISTER_FORM_EMAIL_INPUT_FIELD = (
        By.XPATH,
        '//*[@id="attach_modal"]/section/div/div[2]/div//div[2]/div/div/div/div[1]/div[2]/form/div/div[1]/div/div/div[1]/div[1]')
    REGISTER_FORM_PASSWORD_INPUT_FIELD = (
        By.XPATH,
        "//*[contains(@class, 'v-text-field__slot')][3]")
    REGISTER_FORM_REFERAL_CODE_INPUT_FIELD = (
        By.XPATH,
        "//*[contains(@class, 'v-text-field__slot')][4]")
    REGISTER_FORM_AGREE_CHECKBOX = (
        By.CSS_SELECTOR,
        '[role="checkbox"]')
    REGISTER_FORM_CONTINUE_BUTTON = (
        By.CSS_SELECTOR,
        '[specialtoken="k-btn-long-button"]')


class ExceptionsRegisterFormLocators():
    EXCEPTION_TEXT_USER_NAME_FIELD = (
        By.XPATH,
        "//*[contains(@specialtoken, 'k-text-k-typography-body-2-regular')][1]"
    )
    EXCEPTION_TEXT_EMAIL_FIELD = (
        By.XPATH,
        "//*[contains(@specialtoken, 'k-text-k-typography-body-2-regular')][2]"
    )
    EXCEPTION_TEXT_PASSWORD_FIELD = (
        By.XPATH,
        "//*[contains(@specialtoken, 'k-text-k-typography-body-2-regular')][3]"
    )
    EXCEPTION_TEXT_REFERAL_CODE_FIELD = (
        By.XPATH,
        "//*[contains(@specialtoken, 'k-text-k-typography-body-2-regular')][4]"
    )
    EXCEPTION_TEXT_CHECKBOX = (
        By.CSS_SELECTOR,
        "use[href='#system--default--$checkboxErrorDark']"
    )


class AuthenticationFormLocators():
    AUTHENTICATIOF_FIELD = (
        By.CSS_SELECTOR,
        '[specialtoken="k-text-k-typography-h6-semi-bold,k-text-default"]'
    )