import pytest
from .locators import *

class BasePage():
    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    def open(self):
        self.browser.get(self.url)

    def is_not_element_present(self):
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.presence_of_element_located(AuthenticationFormLocators.AUTHENTICATIOF_FIELD))
        except TimeoutException:
            return True
        return False