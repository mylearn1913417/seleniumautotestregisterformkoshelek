from .base_page import BasePage
from .locators import *
from .valid_data import *
from .invalid_data import *


class RegisterPage(BasePage):

    def enter_checkbox(self):
        agree_checkbox = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_AGREE_CHECKBOX)
        agree_checkbox.click()

    def press_continue(self):
        continue_button = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_CONTINUE_BUTTON)
        continue_button.click()

    def enter_valid_data_from_required_field(self):
        self.enter_valid_user_name()
        self.enter_valid_email()
        self.enter_valid_password()

    def enter_invalid_spase_required_fields(self):
        self.enter_invalid_space_user_name()
        self.enter_invalid_spase_email()
        self.enter_invalid_space_password()

    def enter_invalid_one_char_referal_code(self):
        referal_code_input_field = self.browser.find_element(
            *RegisterFormLocators.REGISTER_FORM_REFERAL_CODE_INPUT_FIELD)
        referal_code_input_field.send_keys(invalid_referal_code_one_char)

    def enter_invalid_exceeding_max_lenth_referal_code(self):
        referal_code_input_field = self.browser.find_element(
            *RegisterFormLocators.REGISTER_FORM_REFERAL_CODE_INPUT_FIELD)
        referal_code_input_field.send_keys(invalid_exceeding_max_lenth_referal_code)

    def enter_invalid_referal_code_many_char(self):
        referal_code_input_field = self.browser.find_element(
            *RegisterFormLocators.REGISTER_FORM_REFERAL_CODE_INPUT_FIELD)
        referal_code_input_field.send_keys(invalid_referal_code_many_char)

    def enter_valid_data_with_existing_user_name_from_required_field(self):
        self.enter_existing_user_name()
        self.enter_valid_email()
        self.enter_valid_password()

    def enter_valid_data_with_existing_email_from_required_field(self):
        self.enter_valid_user_name()
        self.enter_existing_email()
        self.enter_valid_password()

    def enter_valid_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(valid_user_name)

    def enter_valid_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(valid_email)

    def enter_valid_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(valid_password)

    def enter_invalid_space_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_space)

    def enter_invalid_space_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_space)

    def enter_invalid_space_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_space)

    def enter_invalid_rus_letter_capital_lowercase_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_rus_letter_capital_lowercase_number_user_name)

    def enter_invalid_en_letter_lowercase_wihtout_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_letter_lowercase_wihtout_number_user_name)

    def enter_invalid_en_letter_capital_lowercase_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_letter_capital_lowercase_number_user_name)

    def enter_invalid_en_letter_dash_and_symbol_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_letter_dash_and_symbol_user_name)

    def enter_invalid_spec_symbol_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_spec_symbol_user_name)

    def enter_invalid_en_one_letter_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_one_letter_user_name)

    def enter_invalid_one_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_one_number_user_name)

    def enter_invalid_en_exceeding_max_length_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_exceeding_max_length_number_user_name)

    def enter_invalid_en_many_letter_number_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_en_many_letter_number_user_name)

    def enter_invalid_emoji_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(invalid_emoji_user_name)

    def enter_invalid_without_at_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_without_at_email)

    def enter_invalid_without_domen_part_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_without_domen_part_email)

    def enter_invalid_without_local_part_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_without_local_part_email)

    def enter_invalid_with_double_at_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_with_double_at_email)

    def enter_invalid_reserved_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_reserved_email)

    def enter_invalid_nonexistent_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_nonexistent_email)

    def enter_invalid_with_spec_simbol_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_with_spec_simbol_email)

    def enter_invalid_emoji_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_emoji_email)

    def enter_invalid_max_length_local_part_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_max_length_local_part_email)

    def enter_invalid_exceeding_max_lenth_local_part_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_exceeding_max_lenth_local_part_email)

    def enter_invalidl_max_lenth_domen_part_emai(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalidl_max_lenth_domen_part_emai)

    def enter_invalid_exceeding_max_lenth_domen_part_email(self):
        email_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_EMAIL_INPUT_FIELD)
        email_input_field.send_keys(invalid_exceeding_max_lenth_domen_part_email)

    def enter_invalid_short_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_short_password)

    def enter_invalid_no_required_char_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_no_required_char_password)

    def enter_invalid_repeating_char_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_repeating_char_password)

    def enter_invalid_simple_template_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_simple_template_password)

    def enter_invalid_spec_symbol_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_spec_symbol_password)

    def enter_invalid_unacceptable_symbol_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_unacceptable_symbol_password)

    def enter_invalid_exceeding_max_length_password(self):
        password_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_PASSWORD_INPUT_FIELD)
        password_input_field.send_keys(invalid_exceeding_max_length_password)

    def enter_existing_user_name(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(existing_user_name)

    def enter_existing_email(self):
        user_name_input_field = self.browser.find_element(*RegisterFormLocators.REGISTER_FORM_USER_NAME_INPUT_FIELD)
        user_name_input_field.send_keys(existing_email)

    def check_exception_empty_user_name_input_field(self):
        exception_empty_user_name = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_USER_NAME_FIELD)
        assert exception_empty_user_name.text == 'Поле не заполнено', \
            'There is no message about empty username field'

    def check_exception_empty_email_input_field(self):
        exception_empty_email = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_EMAIL_FIELD)
        assert exception_empty_email.text == 'Поле не заполнено', \
            'There is no message about empty email field'

    def check_exception_empty_password_input_field(self):
        exception_empty_password = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_PASSWORD_FIELD)
        assert exception_empty_password.text == 'Поле не заполнено', \
            'There is no message about empty password field'

    def check_exception_empty_agree_checkbox(self):
        assert self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_CHECKBOX), \
            'There is no message about empty checkbox'

    def check_exception_invalid_user_name_input_field(self):
        exception_invalid_user_name = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_USER_NAME_FIELD)
        assert exception_invalid_user_name.text == \
               'Допустимые символы (от 6 до 32): a-z, 0-9, _. Имя должно начинаться с буквы', \
            'There is no message about entering an invalid username'

    def check_exception_invalid_email_input_field(self):
        exception_invalid_email = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_EMAIL_FIELD)
        assert exception_invalid_email.text == 'Формат e-mail: username@test.ru', \
            'There is no message about entering an invalid email'

    def check_exception_invalid_password_input_field(self):
        exception_invalid_password = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_PASSWORD_FIELD)
        assert exception_invalid_password.text == \
               'Пароль должен содержать от 8 до 64 символов, включая заглавные буквы и цифры', \
            'There is no message about entering an invalid password'

    def check_exception_invalid_agree_referal_code(self):
        exception_invalid_agree_referal_code = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_REFERAL_CODE_FIELD)
        assert exception_invalid_agree_referal_code.text == 'Неверный формат ссылки', \
            'There is no message about entering an invalid referal code'

    def check_exception_existing_user_name_input_field(self):
        exception_existing_user_name = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_USER_NAME_FIELD)
        assert exception_existing_user_name == 'Имя пользователя уже занято', \
            'There is no message the username is taken'

    def check_exception_existing_email_input_field(self):
        exception_existing_email = \
            self.browser.find_element(*ExceptionsRegisterFormLocators.EXCEPTION_TEXT_EMAIL_FIELD)
        assert exception_existing_email.text == 'Емейл уже занят', 'There is no message the email is taken'

    def not_go_authentication_page(self):
        assert self.is_not_element_present(AuthenticationFormLocators.AUTHENTICATIOF_FIELD), \
            'Redirect go to authentication page'